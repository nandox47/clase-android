package pe.com.niubiz.myapplication.core.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pe.com.niubiz.myapplication.core.api.ApiService
import pe.com.niubiz.myapplication.core.db.AppDatabase
import pe.com.niubiz.myapplication.core.repositories.ITaskRepository
import pe.com.niubiz.myapplication.core.repositories.TaskRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideTaskRepository(appDatabase: AppDatabase, apiService: ApiService): ITaskRepository =
        TaskRepository(appDatabase, apiService)

}