package pe.com.niubiz.myapplication.core.repositories

import androidx.lifecycle.LiveData
import pe.com.niubiz.myapplication.core.api.ApiService
import pe.com.niubiz.myapplication.core.api.responses.GetProductsResponse
import pe.com.niubiz.myapplication.core.db.AppDatabase
import pe.com.niubiz.myapplication.core.db.entities.Task
import javax.inject.Inject

class TaskRepository @Inject constructor(
    private val appDatabase: AppDatabase,
    private val apiService: ApiService
    //SHARED PREFERENCES
    //API SERVICE
) : ITaskRepository {
    override fun getByIdLive(id: String) = appDatabase.taskDao().getByIdLive(id)

    override suspend fun getById(id: String) = appDatabase.taskDao().getById(id)

    override suspend fun getAll() = appDatabase.taskDao().getAll()

    override fun getAllLive() = appDatabase.taskDao().getAllLive()

    override suspend fun upsertRow(task: Task) {
        appDatabase.taskDao().upsert(task)
    }

    override suspend fun deleteById(id: String) {
        appDatabase.taskDao().deleteById(id)
    }

    override suspend fun deleteAll() {
        appDatabase.taskDao().deleteAll()
    }

    override suspend fun getProducts() = apiService.getProducts()

}

interface ITaskRepository {
    fun getByIdLive(id: String): LiveData<Task?>
    suspend fun getById(id: String): Task?
    suspend fun getAll(): List<Task>
    fun getAllLive(): LiveData<List<Task>>
    suspend fun upsertRow(task: Task)
    suspend fun deleteById(id: String)
    suspend fun deleteAll()
    suspend fun getProducts(): GetProductsResponse
}