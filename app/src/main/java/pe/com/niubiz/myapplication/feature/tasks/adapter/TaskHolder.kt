package pe.com.niubiz.myapplication.feature.tasks.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.viewbinding.BindableItem
import pe.com.niubiz.myapplication.BR
import pe.com.niubiz.myapplication.R
import pe.com.niubiz.myapplication.core.MyApp
import pe.com.niubiz.myapplication.core.db.entities.Task
import pe.com.niubiz.myapplication.databinding.ItemTaskBinding

class TaskViewHolder(
    val task: Task
) : BindableItem<ItemTaskBinding>() {

    override fun bind(viewBinding: ItemTaskBinding, position: Int) {
        viewBinding.apply {
            tvName.text = task.name ?: "--"
            icon.visibility = if (task.status == true) View.VISIBLE else View.INVISIBLE
        }
    }

    override fun getLayout() = R.layout.item_task

    override fun initializeViewBinding(view: View) = ItemTaskBinding.bind(view)

}