package pe.com.niubiz.myapplication.core.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pe.com.niubiz.myapplication.core.repositories.ITaskRepository
import pe.com.niubiz.myapplication.core.use_cases.ITaskUseCase
import pe.com.niubiz.myapplication.core.use_cases.TaskUseCase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    @Singleton
    fun provideTaskUseCase(repository: ITaskRepository): ITaskUseCase =
        TaskUseCase(repository)

}