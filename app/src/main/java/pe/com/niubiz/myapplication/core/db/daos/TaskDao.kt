package pe.com.niubiz.myapplication.core.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pe.com.niubiz.myapplication.core.db.entities.Task

@Dao
interface TaskDao {//CRUD

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(task: Task)//UPDATE-CREATE

    @Query("SELECT * FROM Task")
    suspend fun getAll(): List<Task>//READ

    @Query("SELECT * FROM Task")
    fun getAllLive(): LiveData<List<Task>>//READ-LIVE

    @Query("SELECT * FROM Task WHERE id = :id LIMIT 1")
    suspend fun getById(id: String): Task?//READ

    @Query("SELECT * FROM Task WHERE id = :id LIMIT 1")
    fun getByIdLive(id: String): LiveData<Task?>//READ-LIVE

    @Query("DELETE FROM Task WHERE id = :id")
    suspend fun deleteById(id: String)//DELETE

    @Query("DELETE FROM Task")
    suspend fun deleteAll()//DELETE

}