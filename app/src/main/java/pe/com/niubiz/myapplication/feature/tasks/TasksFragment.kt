package pe.com.niubiz.myapplication.feature.tasks

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.OnItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import pe.com.niubiz.myapplication.core.db.entities.Task
import pe.com.niubiz.myapplication.databinding.FragmentTasksBinding
import pe.com.niubiz.myapplication.feature.tasks.adapter.TaskViewHolder

@AndroidEntryPoint
class TasksFragment : Fragment() {

    private var _binding: FragmentTasksBinding? = null
    private val binding get() = _binding!!

    private val viewModel: TaskViewModel by viewModels()

    private val groupieAdapter = GroupieAdapter()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTasksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBinding()
        setupObservers()
        viewModel.getProducts()
    }

    private fun setupObservers() {
        viewModel.getAllTask().observe(viewLifecycleOwner) { list ->
            fillRecyclerView(list)
        }
    }

    private fun fillRecyclerView(taskList: List<Task>) {
        Log.e("fillRecyclerView", "taskList=${taskList.size}")
        groupieAdapter.updateAsync(taskList.map { TaskViewHolder(it) })
    }

    private fun setupBinding() {
        binding.apply {
            groupieAdapter.setOnItemClickListener(clickListener)
            rvTask.apply {
                adapter = groupieAdapter
                layoutManager = LinearLayoutManager(requireContext())
            }

            btnCreate.setOnClickListener {
                val action = TasksFragmentDirections.actionFragmentTaskToFragmentUpsertTask()
                findNavController().navigate(action)
            }
        }
    }

    private val clickListener = OnItemClickListener { item, _ ->
        if (item is TaskViewHolder) {
            val action = TasksFragmentDirections.actionFragmentTaskToFragmentUpsertTask()
            action.id = item.task.id
            findNavController().navigate(action)
        }
    }

}