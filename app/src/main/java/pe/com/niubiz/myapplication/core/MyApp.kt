package pe.com.niubiz.myapplication.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        myInstance = this
    }

    companion object {
        lateinit var myInstance: MyApp
    }

}