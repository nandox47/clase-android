package pe.com.niubiz.myapplication.core.db

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import javax.inject.Inject

class SharedPreferencesManager @Inject constructor(
    private val context: Context
) {
    private val keyGenParameterSpec = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()

    private val sharedPreferences by lazy {
        EncryptedSharedPreferences.create(
            context,
            "my_app_sp",
            keyGenParameterSpec,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }
    fun sharedPreferences() = sharedPreferences

    private fun getString(preferenceName: String, defaultValue: String): String {
        return sharedPreferences.getString(preferenceName, defaultValue) ?: ""
    }

    private fun setString(preferenceName: String, myValue: String) {
        sharedPreferences.edit()
            .putString(preferenceName, myValue)
            .apply()
    }

}