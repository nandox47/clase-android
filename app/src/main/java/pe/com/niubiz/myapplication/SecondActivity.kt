package pe.com.niubiz.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class SecondActivity : AppCompatActivity() {
    var email = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)


        email = intent.getStringExtra("email") ?: "prueba@gmail.com"
    }

    fun onClickReturn(view: View) {
        finish()
    }
}