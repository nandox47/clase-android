package pe.com.niubiz.myapplication.feature.tasks.upsert

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import pe.com.niubiz.myapplication.R
import pe.com.niubiz.myapplication.databinding.FragmentUpsertTaskBinding

@AndroidEntryPoint
class UpsertTaskFragment : Fragment() {

    private var _binding: FragmentUpsertTaskBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UpsertTaskViewModel by viewModels()

    private val myArgs: UpsertTaskFragmentArgs by navArgs()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpsertTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBinding()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.getTask(myArgs.id).observe(viewLifecycleOwner) { task ->
            task?.let {
                binding.tvTaskId.visibility = View.VISIBLE
                binding.tvTaskId.text = "ID: ${it.id}"
                binding.etTaskName.setText(it.name ?: "")
                binding.swTaskStatus.isChecked = it.status == true
            }
        }

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                UpsertState.Done -> {
                    binding.progressBar.visibility = View.GONE
                    findNavController().popBackStack()
                }

                is UpsertState.Fail -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
                }

                null -> {
                    //NOTHING
                }
            }
        }
    }

    private fun setupBinding() {
        binding.apply {
            val taskId = myArgs.id// 0

            btnSave.setOnClickListener {
                progressBar.visibility = View.VISIBLE
                viewModel.upsertTask(
                    id = taskId,
                    name = etTaskName.text.toString(),
                    status = swTaskStatus.isChecked
                )
            }
        }
    }

}