package pe.com.niubiz.myapplication.core.api

import pe.com.niubiz.myapplication.core.api.responses.GetProductsResponse
import retrofit2.http.GET

interface ApiService {

    @GET("product/20106836.json")
    suspend fun getProducts(): GetProductsResponse

}