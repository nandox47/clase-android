package pe.com.niubiz.myapplication.core.use_cases

import androidx.lifecycle.LiveData
import pe.com.niubiz.myapplication.core.api.responses.GetProductsResponse
import pe.com.niubiz.myapplication.core.db.entities.Task
import pe.com.niubiz.myapplication.core.repositories.ITaskRepository
import javax.inject.Inject

class TaskUseCase @Inject constructor(
    private val repository: ITaskRepository
) : ITaskUseCase {
    override fun getAllLive() = repository.getAllLive()

    override suspend fun upsert(id: String, name: String, status: Boolean) {
        var task = Task()
        if (id != "0") task = repository.getById(id) ?: Task()
        task.name = name
        task.status = status
        repository.upsertRow(task)
    }

    override fun getById(id: String) = repository.getByIdLive(id)
    override suspend fun getProducts() = repository.getProducts()
}

interface ITaskUseCase {
    fun getAllLive(): LiveData<List<Task>>//READ
    suspend fun upsert(id: String, name: String, status: Boolean)//CREATE - UPDATE
    fun getById(id: String): LiveData<Task?>
    suspend fun getProducts(): GetProductsResponse
}