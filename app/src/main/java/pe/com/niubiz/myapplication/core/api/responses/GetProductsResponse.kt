package pe.com.niubiz.myapplication.core.api.responses

class GetProductsResponse(
    val status: Int,
    val code: String,
    val status_verbose: String,
    val product: ProductResponse
)

class ProductResponse(
    val cities_tags: List<Any>,
    val image_nutrition_thumb_url: String?,
)