package pe.com.niubiz.myapplication.core.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity
data class Task(
    @PrimaryKey val id: String = UUID.randomUUID().toString(),
    var name: String? = null,//"Mate2"
    var status: Boolean? = null//false
)