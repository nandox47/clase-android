package pe.com.niubiz.myapplication.core.db

import androidx.room.Database//SQLite
import androidx.room.RoomDatabase
import pe.com.niubiz.myapplication.core.db.daos.TaskDao
import pe.com.niubiz.myapplication.core.db.entities.Task

@Database(
    entities = [
        Task::class
    ],
    version = 2
)
abstract class AppDatabase: RoomDatabase() {
    //DAO
    abstract fun taskDao(): TaskDao
    //Migrations
}