package pe.com.niubiz.myapplication.feature.tasks.upsert

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.com.niubiz.myapplication.core.use_cases.ITaskUseCase
import javax.inject.Inject

@HiltViewModel
class UpsertTaskViewModel @Inject constructor(
    private val useCase: ITaskUseCase
) : ViewModel() {

    private var _state = MutableLiveData<UpsertState?>(null)
    val state: LiveData<UpsertState?> get() = _state

    fun getTask(id: String) = useCase.getById(id)

    fun upsertTask(id: String, name: String, status: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            useCase.upsert(id = id, name = name, status = status)
            _state.postValue(UpsertState.Done)
        }.onFailure {
            it.printStackTrace()
            _state.postValue(UpsertState.Fail(message = it.message ?: "ERROR DESCONOCIDO"))
        }
    }

}

sealed class UpsertState {
    object Done : UpsertState()
    class Fail(val message: String) : UpsertState()
}