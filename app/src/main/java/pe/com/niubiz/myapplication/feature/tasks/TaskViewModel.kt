package pe.com.niubiz.myapplication.feature.tasks

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.com.niubiz.myapplication.core.use_cases.ITaskUseCase
import javax.inject.Inject

@HiltViewModel
class TaskViewModel @Inject constructor(
    private val useCase: ITaskUseCase
): ViewModel() {

    fun getAllTask() = useCase.getAllLive()
    fun getProducts() = viewModelScope.launch(Dispatchers.IO) {
        runCatching {
            val response = useCase.getProducts()
            //state(response)
            Log.e("RESPONSE", "=$response")
        }.onFailure {
            it.printStackTrace()
        }
    }

}