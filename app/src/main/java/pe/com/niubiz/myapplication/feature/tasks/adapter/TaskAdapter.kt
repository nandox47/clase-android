package pe.com.niubiz.myapplication.feature.tasks.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pe.com.niubiz.myapplication.R
import pe.com.niubiz.myapplication.databinding.ItemTaskBinding

//class TaskAdapter (
//    private val list: List<Task>
//): RecyclerView.Adapter<TaskHolder>() {
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
//        val layoutInflater = LayoutInflater.from(parent.context)
//        val binding: ItemTaskBinding = DataBindingUtil.inflate(
//            layoutInflater, R.layout.item_task, parent, false
//        )
//        return TaskHolder(binding)
//    }
//
//    override fun getItemCount() = list.count()
//
//    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
//        val task = list[position]
//        holder.bind(task)
//    }
//}